#!/usr/bin/python3

import sys
import os
import getopt
import yaml
from string import Template


try:
  opts, args = getopt.getopt(sys.argv[1:], "c:t:", ["config=", "template="])
except getopt.GetoptError:
  print('generateJobs.py -c <configfile> -t <templatefile>')
  sys.exit(1)

env = os.getenv('PREFIX', 'dev')
config_name = None
template_name = 'advisories.nomad.tpl'
for opt, arg in opts:
  if opt in ("-c", "--config"):
    config_name = arg
  elif opt in ("-t", "--template"):
    template_name = arg

if not config_name:
  print('Missing config file')
  sys.exit(1)

with open(config_name, 'r') as configfile:
  config = yaml.safe_load(configfile)

with open(template_name, 'r') as templatefile:
  template = Template(templatefile.read())

try:
  def_schedule   = config['defaults'].get('schedule')
  def_advisories = config['defaults'].get('advisories')
  def_data       = config['defaults'].get('data')
  def_repos      = config['defaults'].get('repos', [])
except IndexError:
  print('Missing configuration options')
  sys.exit(2)

del config['defaults']

for job in config:
  ARCHES = config[job].get('arches', ['x86_64'])

  for arch in ARCHES:
    repos = [r.replace('$ARCH', arch) for r in config[job].get('repos', def_repos)]

    data = {
      'JOB'        : job,
      'ARCH'       : arch,
      'SCHEDULE'   : config[job].get('schedule', def_schedule),
      'ADVISORIES' : config[job].get('advisories', def_advisories),
      'DATA'       : config[job].get('data', def_data),
      'REPOS'      : ' '.join(repos),
    }

    jobfile = f'{env}_{job}_{arch}_advisories.nomad'
    with open(jobfile, 'w') as jobf:
      jobf.write(template.safe_substitute(data))
      print(f'Generated job: {jobfile}')
