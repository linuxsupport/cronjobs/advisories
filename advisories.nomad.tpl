job "${PREFIX}_${JOB}_${ARCH}_advisories" {
  datacenters = ["*"]

  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  task "${PREFIX}_${JOB}_${ARCH}_advisories" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/advisories/advisories:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_${JOB}_${ARCH}_advisories"
        }
      }
      volumes = [
        "$ADVISORIES:/advisories",
        "$DATA:/data:ro",
      ]
    }

    env {
      DIST = "$JOB"
      ARCH = "$ARCH"
      REPOS = "$REPOS"
      TAG = "${PREFIX}_${JOB}_${ARCH}_advisories"
    }

    resources {
      cpu = 4000 # Mhz
      memory = 400 # MB
    }

  }
}
