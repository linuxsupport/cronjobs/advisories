#!/bin/bash

TOP="/advisories"
ROOT="/data"
UPDINFOPY="/root/updateinfo.py"

mkdir -p $TOP/$DIST/$ARCH/
for REPO in $REPOS; do
  if [ ! -d ${ROOT}/${REPO} ]; then
    echo "repo ${ROOT}/${REPO} does not exist"
    continue
  fi

  echo "processing: ${ROOT}/$REPO"
  UPXMLFILE=$(sed '/updateinfo.xml.gz/!d; s/.*\/\(.*\)".*/\1/' ${ROOT}/${REPO}/repodata/repomd.xml)
  if [ -f ${ROOT}/${REPO}/repodata/$UPXMLFILE ]; then
    XMLFILE="${ROOT}/${REPO}/repodata/$UPXMLFILE"
  elif [ -f ${ROOT}/$REPO/updateinfo.xml ]; then
    XMLFILE="${ROOT}/$REPO/updateinfo.xml"
  else
    echo "no xml file in ${ROOT}/$REPO"
    continue
  fi

  echo "found: ${XMLFILE}"
  echo "$UPDINFOPY --arch $ARCH --file ${XMLFILE} --database $TOP/$DIST/$ARCH/updateinfo.db.new --updatedb"
  $UPDINFOPY --arch $ARCH --file ${XMLFILE} --database $TOP/$DIST/$ARCH/updateinfo.db.new --updatedb
done

# Replace the old database with the new one
mv -v $TOP/$DIST/$ARCH/updateinfo.db.new $TOP/$DIST/$ARCH/updateinfo.db
echo "done."
