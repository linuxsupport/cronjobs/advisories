#!/usr/bin/env python3
"""Update advisories info"""

import re
from argparse import ArgumentParser
from updatesqlite import SQLiteQueries
import createrepo_c as cr

def updatedbinfo(pargs, advisory_db):
    """Update advisories info"""
    uinfo = cr.UpdateInfo(pargs.updinfofile)
    # updates
    for update in uinfo.updates:
        # errata table
        erratadt = advisory_db.select(table='errata', advisory=update.id, type=update.type)
        if erratadt:
            erratadesc = erratadt[0][2]
            if erratadesc != update.description:
                # update the type and description of the advisor
                advisory_db.update(table='errata', advisory=update.id, type=update.type,
                                   description=update.description)
        else:
            # insert the advisor into the table
            advisory_db.insert(table='errata', advisory=update.id, type=update.type,
                               description=update.description)

        # channelerrata table
        if advisory_db.select(table='channelerrata', advisory=update.id,
                              synopsis=update.title):
            # update the issue_date and update_date
            advisory_db.update(table='channelerrata', advisory=update.id,
                               synopsis=update.title, issued_date=update.issued_date,
                               updated_date=update.updated_date)
        else:
            # insert the advisor into the table
            advisory_db.insert(table='channelerrata', advisory=update.id,
                               synopsis=update.title, issued_date=update.issued_date,
                               updated_date=update.updated_date)

        # references
        for ref in update.references:
            if ref.type == "bugzilla":
                bugzdata = advisory_db.select(table='erratabugzilla',
                                              advisory=update.id, bugzilla_id=ref.id)
                if bugzdata:
                    summary = bugzdata[0][1]
                    if summary != ref.title:
                        # update the summary of bugzilla id
                        advisory_db.update(table='erratabugzilla', advisory=update.id,
                                           bugzilla_id=ref.id, summary=ref.title)
                else:
                    # insert bugzilla id info
                    advisory_db.insert(table='erratabugzilla', advisory=update.id,
                                       bugzilla_id=ref.id, summary=ref.title)
            if ref.type == "cve":
                if not advisory_db.select(table='erratacves', advisory=update.id,
                                          cve_id=ref.id):
                    # insert cve id info
                    advisory_db.insert(table='erratacves', advisory=update.id,
                                       cve_id=ref.id)

        # collections
        for col in update.collections:
            misc_data = advisory_db.select(table='misc')
            if not misc_data:
                # insert in misc
                advisory_db.insert(table='misc', advisory=1, channelname=col.name)
            else:
                if misc_data[0][0] != col.name:
                    advisory_db.update(table='misc', channelname=col.name)

            for pkg in col.packages:
                if re.match("noarch|%s" % pargs.archlabel, pkg.arch):
                    advisory_db.insert(table='erratapackages', advisory=update.id,
                                       pkgname=pkg.name, pkgversion=pkg.version,
                                       release=pkg.release, epoch=pkg.epoch,
                                       arch_label=pkg.arch, file=pkg.filename,
                                       md5sum=pkg.sum)

def main():
    """Main function to parse the advisories arguments"""
    parser = ArgumentParser(description="Advisores - update information")
    parser.add_argument('--file', metavar="FILE", dest="updinfofile",
                        help="Filename to update information")
    parser.add_argument('--database', metavar="FILE", dest="dbfile",
                        help="Database file")
    parser.add_argument('--arch', dest='archlabel',
                        help="Arch")
    parser.add_argument('--updatedb', action="store_true", default=False, dest='updatedb',
                        help="Update the database with the xml data")
    pargs = parser.parse_args()

    if pargs.archlabel == 'i686':
        pargs.archlabel = "i386|i486|i586|i686"

    # Create tables if they do not exist
    db = SQLiteQueries(pargs.dbfile)
    db.connect_db()
    db.create_tables()

    if pargs.updatedb:
        updatedbinfo(pargs, db)
    
    db.close_connection()


if __name__ == '__main__':
    main()
